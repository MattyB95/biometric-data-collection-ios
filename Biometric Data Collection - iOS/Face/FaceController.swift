import UIKit

class FaceController: UIViewController {

    private let modality = "Face"
    private let minimumAttemptNumber = 5
    private let cameraDispatchGroup = DispatchGroup()
    private let biometricDispatchGroup = DispatchGroup()

    private var biometricHandler: BiometricHandler?
    private var cameraHandler: CameraHandler?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(coder aDecoder: NSCoder, scenario: String) {
        biometricHandler = BiometricHandler(modality: modality, scenario: scenario)
        cameraHandler = CameraHandler(modality: modality, scenario: scenario);
        super.init(coder: aDecoder)!
    }

    func takePhoto(counterLabel: UILabel, nextButton: UIButton) {
        let incrementCounter = Int(counterLabel.text!)! + 1
        cameraDispatchGroup.enter()
        let camera = cameraHandler!.presentCamera(attemptNumber: incrementCounter, dispatchGroup: cameraDispatchGroup)
        if DeviceHandler.isFaceID() {
            handleFaceID(camera: camera, counterLabel: counterLabel, nextButton: nextButton, incrementCounter: incrementCounter)
        } else {
            present(camera, animated: true)
            cameraDispatchGroup.notify(queue: .main) {
                self.updateCounterView(counterLabel: counterLabel, counterValue: incrementCounter)
                self.showNextButton(nextButton: nextButton, counterValue: incrementCounter)
            }
        }
    }

    private func handleFaceID(camera: UIImagePickerController, counterLabel: UILabel, nextButton: UIButton, incrementCounter: Int) {
        present(camera, animated: true)
        biometricDispatchGroup.enter()
        cameraDispatchGroup.notify(queue: .main) {
            self.biometricHandler!.startBiometricAuthentication(attemptNumber: incrementCounter, dispatchGroup: self.biometricDispatchGroup)
        }
        biometricDispatchGroup.notify(queue: .main) {
            self.updateCounterView(counterLabel: counterLabel, counterValue: incrementCounter)
            self.showNextButton(nextButton: nextButton, counterValue: incrementCounter)
        }
    }

    private func updateCounterView(counterLabel: UILabel, counterValue: Int) {
        counterLabel.text = String(counterValue)
    }

    private func showNextButton(nextButton: UIButton, counterValue: Int) {
        if counterValue >= minimumAttemptNumber {
            nextButton.isHidden = false
        }
    }

}

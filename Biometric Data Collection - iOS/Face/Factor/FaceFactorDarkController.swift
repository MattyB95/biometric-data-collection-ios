import UIKit

class FaceFactorDarkController: FaceController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Factor - Dark"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func takePhoto(_ sender: Any) {
        takePhoto(counterLabel: counterLabel, nextButton: nextButton)
    }

}

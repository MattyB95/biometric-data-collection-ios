import UIKit

class FaceSittingController: FaceController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Sitting"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func takePhoto(_ sender: UIButton) {
        takePhoto(counterLabel: counterLabel, nextButton: nextButton)
    }

}

import UIKit

class FaceStandingController: FaceController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Standing"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func takePhoto(_ sender: Any) {
        takePhoto(counterLabel: counterLabel, nextButton: nextButton)
    }

}

import UIKit

class FingerprintCorridorController: FingerprintController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Corridor"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func biometricAuthentication(_ sender: Any) {
        biometricAuthentication(counterLabel: counterLabel, nextButton: nextButton)
    }

}

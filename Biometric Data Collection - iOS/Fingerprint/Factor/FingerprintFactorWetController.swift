import UIKit

class FingerprintFactorWetController: FingerprintController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Factor - Wet"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func biometricAuthentication(_ sender: Any) {
        biometricAuthentication(counterLabel: counterLabel, nextButton: nextButton)
    }

}

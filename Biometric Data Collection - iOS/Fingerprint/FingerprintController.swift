import UIKit

class FingerprintController: UIViewController {

    private let modality = "Fingerprint"
    private let minimumAttemptNumber = 5
    private let biometricDispatchGroup = DispatchGroup()

    private var biometricHandler: BiometricHandler?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(coder aDecoder: NSCoder, scenario: String) {
        biometricHandler = BiometricHandler(modality: modality, scenario: scenario)
        super.init(coder: aDecoder)!
    }

    func biometricAuthentication(counterLabel: UILabel, nextButton: UIButton) {
        let incrementCounter = Int(counterLabel.text!)! + 1
        biometricDispatchGroup.enter()
        biometricHandler!.startBiometricAuthentication(attemptNumber: incrementCounter, dispatchGroup: biometricDispatchGroup)
        updateCounterView(counterLabel: counterLabel, counterValue: incrementCounter)
        showNextButton(nextButton: nextButton, counterValue: incrementCounter)
    }

    private func updateCounterView(counterLabel: UILabel, counterValue: Int) {
        counterLabel.text = String(counterValue)
    }

    private func showNextButton(nextButton: UIButton, counterValue: Int) {
        if counterValue >= minimumAttemptNumber {
            nextButton.isHidden = false
        }
    }

}

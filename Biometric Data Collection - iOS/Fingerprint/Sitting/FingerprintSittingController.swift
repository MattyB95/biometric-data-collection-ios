import UIKit

class FingerprintSittingController: FingerprintController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Sitting"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func biometricAuthentication(_ sender: Any) {
        biometricAuthentication(counterLabel: counterLabel, nextButton: nextButton)
    }

}

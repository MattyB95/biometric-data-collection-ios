import UIKit

class FingerprintTreadmillController: FingerprintController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Treadmill"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func biometricAuthentication(_ sender: Any) {
        biometricAuthentication(counterLabel: counterLabel, nextButton: nextButton)
    }

}

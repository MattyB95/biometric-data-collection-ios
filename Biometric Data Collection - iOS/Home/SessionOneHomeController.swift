import UIKit

class HomeController: UIViewController {

    @IBOutlet weak var fingerprintOption: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if DeviceHandler.isFaceID() {
            fingerprintOption.isHidden = true
        }
    }

}

import UIKit

class IrisController: UIViewController {

    public static let modality = "Iris"

    private let minimumAttemptNumber = 5
    private let cameraDispatchGroup = DispatchGroup()

    private var cameraHandler: CameraHandler?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(coder aDecoder: NSCoder, scenario: String) {
        cameraHandler = CameraHandler(modality: IrisController.modality, scenario: scenario)
        super.init(coder: aDecoder)!
    }

    func irisAuthenticate(counterLabel: UILabel, nextButton: UIButton) {
        let incrementCounter = Int(counterLabel.text!)! + 1
        cameraDispatchGroup.enter()
        let camera = cameraHandler!.presentCamera(attemptNumber: incrementCounter, dispatchGroup: cameraDispatchGroup)
        present(camera, animated: true)
        cameraDispatchGroup.notify(queue: .main) {
            self.updateCounterView(counterLabel: counterLabel, counterValue: incrementCounter)
            self.showNextButton(nextButton: nextButton, counterValue: incrementCounter)
        }
    }

    private func updateCounterView(counterLabel: UILabel, counterValue: Int) {
        counterLabel.text = String(counterValue)
    }

    private func showNextButton(nextButton: UIButton, counterValue: Int) {
        if counterValue >= minimumAttemptNumber {
            nextButton.isHidden = false
        }
    }

}

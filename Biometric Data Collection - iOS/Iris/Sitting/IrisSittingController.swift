import UIKit

class IrisSittingController: IrisController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Sitting"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func irisAuthenticate(_ sender: Any) {
        irisAuthenticate(counterLabel: counterLabel, nextButton: nextButton)
    }

}

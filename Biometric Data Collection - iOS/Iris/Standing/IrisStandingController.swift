import UIKit

class IrisStandingController: IrisController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Standing"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func irisAuthenticate(_ sender: Any) {
        irisAuthenticate(counterLabel: counterLabel, nextButton: nextButton)
    }

}

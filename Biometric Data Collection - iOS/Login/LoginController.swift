import UIKit

class LoginController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var participantIdentifierTextField: UITextField!

    public static let participantIdentifierKey = "ParticipantIdentifier"

    private let debugUser = "Debug"
    private let sessionPickerStoryboard = "SessionPicker"

    override func viewDidLoad() {
        super.viewDidLoad()
        participantIdentifierTextField.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        participantIdentifierTextField.resignFirstResponder()
        assignParticipantIdentifierToUserDefaults()
        startHome()
        return true
    }

    @IBAction func beginButton(_ sender: Any) {
        assignParticipantIdentifierToUserDefaults()
        startHome()
    }

    private func assignParticipantIdentifierToUserDefaults() {
        let participantIdentifier = (participantIdentifierTextField.text!.isEmpty) ? debugUser : participantIdentifierTextField.text
        UserDefaults.standard.set(participantIdentifier, forKey: LoginController.participantIdentifierKey)
    }

    private func startHome() {
        let storyboard = UIStoryboard(name: sessionPickerStoryboard, bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()!
        present(viewController, animated: true, completion: nil)
    }

}

import UIKit

class FaceTourController: FaceController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private var scenario = "Tour"

    required init?(coder aDecoder: NSCoder) {
        let stopNumber = UserDefaults.standard.string(forKey: TourStopController.stopNumber)!
        scenario += " - " + stopNumber
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func takePhoto(_ sender: UIButton) {
        takePhoto(counterLabel: counterLabel, nextButton: nextButton)
    }

}

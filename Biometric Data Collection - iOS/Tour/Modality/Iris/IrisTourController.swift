import UIKit

class IrisTourController: IrisController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private var scenario = "Tour"
    private var stopNumber = "0"

    required init?(coder aDecoder: NSCoder) {
        stopNumber = UserDefaults.standard.string(forKey: TourStopController.stopNumber)!
        scenario += " - " + stopNumber
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func irisAuthenticate(_ sender: Any) {
        irisAuthenticate(counterLabel: counterLabel, nextButton: nextButton)
    }

    @IBAction func showNextStop(_ sender: Any) {
        let nextStoryboard = TourStopController.getNextStoryboard(currentStopNumber: stopNumber)
        let viewController = nextStoryboard.instantiateInitialViewController()!
        present(viewController, animated: true, completion: nil)
    }

}

import UIKit

class VoiceTourController: VoiceController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var voicePhrase: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!

    private var scenario = "Tour"

    required init?(coder aDecoder: NSCoder) {
        let stopNumber = UserDefaults.standard.string(forKey: TourStopController.stopNumber)!
        scenario += " - " + stopNumber
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func recordAudio(_ sender: Any) {
        recordAudio(counterLabel: counterLabel, voicePhrase: voicePhrase, recordButton: recordButton, nextButton: nextButton)
    }

}

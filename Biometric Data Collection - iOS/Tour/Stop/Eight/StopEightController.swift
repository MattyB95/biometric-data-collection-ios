import UIKit

class StopEightController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "8")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

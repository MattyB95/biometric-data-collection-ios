import UIKit

class StopFiveController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "5")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

import UIKit

class StopFourController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "4")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

import UIKit

class StopNineController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "9")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

import UIKit

class StopOneController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "1")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

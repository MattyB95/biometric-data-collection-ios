import UIKit

class StopSixController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "6")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

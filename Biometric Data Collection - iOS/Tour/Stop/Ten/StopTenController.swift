import UIKit

class StopTenController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "10")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

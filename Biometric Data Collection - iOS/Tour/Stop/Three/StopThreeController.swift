import UIKit

class StopThreeController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "3")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

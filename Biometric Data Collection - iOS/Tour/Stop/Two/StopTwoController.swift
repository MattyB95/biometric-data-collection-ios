import UIKit

class StopTwoController: TourStopController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, stopNumber: "2")
    }

    @IBAction func arrived(_ sender: Any) {
        super.arrived()
    }

}

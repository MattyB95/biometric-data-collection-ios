import UIKit

class TourStopController: UIViewController {

    public static let stopNumber = "StopNumber"

    private let fingerprintTourStoryboard = "FingerprintTour"
    private let faceTourStoryboard = "FaceTour"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(coder aDecoder: NSCoder, stopNumber: String) {
        UserDefaults.standard.set(stopNumber, forKey: TourStopController.stopNumber)
        super.init(coder: aDecoder)!
    }

    func arrived() {
        var storyboard = UIStoryboard(name: fingerprintTourStoryboard, bundle: nil)
        if DeviceHandler.isFaceID() {
            storyboard = UIStoryboard(name: faceTourStoryboard, bundle: nil)
        }
        let viewController = storyboard.instantiateInitialViewController()!
        present(viewController, animated: true, completion: nil)
    }

    public static func getNextStoryboard(currentStopNumber: String) -> UIStoryboard {
        let nextTourNumber = Int(currentStopNumber)! + 1
        switch (nextTourNumber) {
        case 2:
            return UIStoryboard(name: "StopTwo", bundle: nil)
        case 3:
            return UIStoryboard(name: "StopThree", bundle: nil)
        case 4:
            return UIStoryboard(name: "StopFour", bundle: nil)
        case 5:
            return UIStoryboard(name: "StopFive", bundle: nil)
        case 6:
            return UIStoryboard(name: "StopSix", bundle: nil)
        case 7:
            return UIStoryboard(name: "StopSeven", bundle: nil)
        case 8:
            return UIStoryboard(name: "StopEight", bundle: nil)
        case 9:
            return UIStoryboard(name: "StopNine", bundle: nil)
        case 10:
            return UIStoryboard(name: "StopTen", bundle: nil)
        default:
            return UIStoryboard(name: "TourFinished", bundle: nil)
        }
    }

}

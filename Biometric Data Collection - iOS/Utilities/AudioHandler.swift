import AVFoundation

class AudioHandler: NSObject, AVAudioRecorderDelegate {

    private let dataHandler: DataHandler
    private let sensorService: SensorService
    private let modality: String
    private let underscore = "_"
    private let fileExtension = ".m4a"
    private let elapsedTimeFileName = "ElapsedTime"

    private var audioRecorder: AVAudioRecorder!
    private var attemptNumber = Int()
    private var startTime = UInt64()

    init(modality: String, scenario: String) {
        dataHandler = DataHandler(modality: modality, scenario: scenario)
        sensorService = SensorService(dataHandler: dataHandler)
        self.modality = modality
        let recordingSession = AVAudioSession.sharedInstance()
        try! recordingSession.setCategory(.record, mode: .default)
        try! recordingSession.setActive(true)
        recordingSession.requestRecordPermission { _ in
        }
    }

    func startRecording(attemptNumber: Int) {
        self.attemptNumber = attemptNumber
        let fileName = dataHandler.participantIdentifier + underscore + modality + underscore + String(Date().timeIntervalSince1970)
        let audioFilename = dataHandler.getAttemptFilePath(attemptNumber: attemptNumber, fileName: fileName, fileExtension: fileExtension)
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 48000,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue
        ]
        audioRecorder = try! AVAudioRecorder(url: audioFilename, settings: settings)
        audioRecorder.delegate = self
        sensorService.startAllSensors(attemptNumber: attemptNumber)
        startTime = DispatchTime.now().uptimeNanoseconds
        audioRecorder.record()
    }

    func stopRecording() {
        audioRecorder.stop()
        audioRecorder = nil
        let endTime = DispatchTime.now().uptimeNanoseconds
        sensorService.stopAllSensors()
        sensorService.saveAllSensorData()
        let elapsedTime = endTime - startTime
        let timestamp = Date().timeIntervalSince1970
        let metaData = [elapsedTime, timestamp] as Array<Any>
        dataHandler.saveDataToFile(attemptNumber: attemptNumber, fileName: elapsedTimeFileName, arrayData: metaData)
    }

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            stopRecording()
        }
    }

}

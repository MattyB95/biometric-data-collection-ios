import LocalAuthentication

class BiometricHandler {

    private let authenticate = "Authenticate"

    private let dataHandler: DataHandler
    private let sensorService: SensorService

    private var attemptNumber = Int()
    private var startTime = UInt64()

    private var dispatchGroup: DispatchGroup?

    init(modality: String, scenario: String) {
        dataHandler = DataHandler(modality: modality, scenario: scenario)
        sensorService = SensorService(dataHandler: dataHandler)
    }

    func startBiometricAuthentication(attemptNumber: Int, dispatchGroup: DispatchGroup) {
        self.dispatchGroup = dispatchGroup
        let lAContext = LAContext()
        if lAContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: .none) {
            self.attemptNumber = attemptNumber
            sensorService.startAllSensors(attemptNumber: attemptNumber)
            startTime = DispatchTime.now().uptimeNanoseconds
            lAContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: authenticate, reply: authenticationResult())
        }
    }

    private func authenticationResult() -> (Bool, Error?) -> () {
        { success, error in
            let endTime = DispatchTime.now().uptimeNanoseconds
            self.sensorService.stopAllSensors()
            self.sensorService.saveAllSensorData()
            var data = Array<Any>()
            data.append(self.checkResult(success: success, error: error))
            let elapsedTime = endTime - self.startTime
            data.append(elapsedTime)
            data.append(Date().timeIntervalSince1970)
            self.dataHandler.saveDataToFile(attemptNumber: self.attemptNumber, fileName: self.authenticate, arrayData: data)
            self.dispatchGroup!.leave()
        }
    }

    private func checkResult(success: Bool, error: Error?) -> String {
        if success {
            return "Success"
        } else {
            return error?.localizedDescription ?? "Failure"
        }
    }

}

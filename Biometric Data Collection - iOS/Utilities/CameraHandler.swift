import UIKit
import AVFoundation

class CameraHandler: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    private let elapsedTimeFileName = "ElapsedTime"
    private let underscore = "_"

    private let dataHandler: DataHandler
    private let sensorService: SensorService
    private let modality: String

    private var camera = UIImagePickerController()
    private var attemptNumber = Int()
    private var startTime = UInt64()

    private var dispatchGroup: DispatchGroup?

    init(modality: String, scenario: String) {
        dataHandler = DataHandler(modality: modality, scenario: scenario)
        sensorService = SensorService(dataHandler: dataHandler)
        self.modality = modality
        super.init()
        camera = setupCamera()
    }

    private func setupCamera() -> UIImagePickerController {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraImage = UIImagePickerController()
            cameraImage.delegate = self
            cameraImage.allowsEditing = false
            cameraImage.sourceType = .camera
            cameraImage.cameraCaptureMode = .photo
            cameraImage.cameraFlashMode = .off
            cameraImage.cameraDevice = .front
            return cameraImage
        }
        return UIImagePickerController()
    }

    func presentCamera(attemptNumber: Int, dispatchGroup: DispatchGroup) -> UIImagePickerController {
        self.dispatchGroup = dispatchGroup
        self.attemptNumber = attemptNumber
        if useSensors() {
            sensorService.startAllSensors(attemptNumber: attemptNumber)
            startTime = DispatchTime.now().uptimeNanoseconds
        }
        return camera
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        let timestamp = Date().timeIntervalSince1970
        if useSensors() {
            let endTime = DispatchTime.now().uptimeNanoseconds
            sensorService.stopAllSensors()
            sensorService.saveAllSensorData()
            let elapsedTime = endTime - startTime
            let metaData = [elapsedTime, timestamp] as Array<Any>
            dataHandler.saveDataToFile(attemptNumber: attemptNumber, fileName: elapsedTimeFileName, arrayData: metaData)
        }
        let fileName = modality + underscore + String(timestamp)
        let capturedImage = info[.originalImage] as! UIImage
        dataHandler.saveImage(attemptNumber: attemptNumber, fileName: fileName, image: capturedImage)
        dispatchGroup!.leave()
        picker.dismiss(animated: true)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if DeviceHandler.isTouchID() {
            sensorService.stopAllSensors()
            sensorService.saveAllSensorData()
        }
    }

    private func useSensors() -> Bool {
        DeviceHandler.isTouchID() || modality == IrisController.modality
    }

}

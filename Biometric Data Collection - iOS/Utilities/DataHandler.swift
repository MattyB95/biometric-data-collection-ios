import UIKit

class DataHandler {

    let participantIdentifier = UserDefaults.standard.string(forKey: LoginController.participantIdentifierKey)!

    private let fileManager = FileManager.default
    private let forwardSlash = "/"
    private let underscore = "_"
    private let comma = ","
    private let newLine = "\n"

    private var directoryPath = String()

    init(modality: String, scenario: String) {
        directoryPath = participantIdentifier
        createDirectory(subDirectory: directoryPath)
        directoryPath.append(forwardSlash + modality)
        createDirectory(subDirectory: directoryPath)
        directoryPath.append(forwardSlash + scenario)
        createDirectory(subDirectory: directoryPath)
        directoryPath.append(forwardSlash)
    }

    private func getDocumentsDirectory() -> URL? {
        fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
    }

    private func createDirectory(subDirectory: String) {
        if let documentDirectory = getDocumentsDirectory() {
            let fileURL = documentDirectory.appendingPathComponent(subDirectory)
            if !fileManager.fileExists(atPath: fileURL.path) {
                try! fileManager.createDirectory(atPath: fileURL.path, withIntermediateDirectories: true, attributes: nil)
            }
        }
    }

    func getAttemptFilePath(attemptNumber: Int, fileName: String, fileExtension: String) -> URL {
        createDirectory(subDirectory: directoryPath + String(attemptNumber))
        let documentsDirectory = getDocumentsDirectory()
        let filePath = directoryPath + String(attemptNumber) + forwardSlash + fileName + fileExtension
        return documentsDirectory!.appendingPathComponent(filePath)
    }

    func saveDataToFile(attemptNumber: Int, fileName: String, arrayData: Array<Any>) {
        let fileExtension = ".csv"
        let filePath = getAttemptFilePath(attemptNumber: attemptNumber, fileName: fileName, fileExtension: fileExtension)
        let formattedData = formatArrayData(arrayData: arrayData)
        writeToFile(text: formattedData, fileURL: filePath)
    }

    func saveSensorDataToFile(attemptNumber: Int, fileName: String, arrayData: Array<Array<Double>>) {
        let fileExtension = ".csv"
        let filePath = getAttemptFilePath(attemptNumber: attemptNumber, fileName: fileName, fileExtension: fileExtension)
        let formattedData = formatTwoDimensionalArrayData(twoDimensionalArrayData: arrayData)
        writeToFile(text: formattedData, fileURL: filePath)
    }

    func saveImage(attemptNumber: Int, fileName: String, image: UIImage) {
        createDirectory(subDirectory: directoryPath + String(attemptNumber))
        let compressionQuality = CGFloat(1)
        let fileExtension = ".jpg"
        let imageData = image.jpegData(compressionQuality: compressionQuality)
        let documentsDirectory = getDocumentsDirectory()
        let filePath = directoryPath + String(attemptNumber) + forwardSlash + participantIdentifier + underscore + fileName + fileExtension
        let fileUrl = documentsDirectory!.appendingPathComponent(filePath)
        try! imageData!.write(to: fileUrl)
    }

    private func writeToFile(text: String, fileURL: URL) {
        try! text.write(to: fileURL, atomically: false, encoding: .utf8)
    }

    private func formatArrayData(arrayData: Array<Any>) -> String {
        var formatted = String()
        for element in arrayData {
            formatted.append(checkElementType(element: element))
            formatted.append(comma)
        }
        formatted.removeLast()
        formatted.append(newLine)
        return formatted
    }

    private func checkElementType(element: Any) -> String {
        switch element {
        case let doubleElement as Double:
            return String(doubleElement)
        case let uInt64Element as UInt64:
            return String(uInt64Element)
        default:
            return element as! String
        }
    }

    private func formatTwoDimensionalArrayData(twoDimensionalArrayData: Array<Array<Double>>) -> String {
        var formatted = String()
        for entry in twoDimensionalArrayData {
            formatted.append(formatArrayData(arrayData: entry))
        }
        return formatted
    }

}

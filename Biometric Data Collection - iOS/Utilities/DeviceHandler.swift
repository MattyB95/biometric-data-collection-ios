import LocalAuthentication

class DeviceHandler {

    static let laContext = LAContext()

    static func isTouchID() -> Bool {
        if laContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: .none) {
            return laContext.biometryType == LABiometryType.touchID
        }
        return false
    }

    static func isFaceID() -> Bool {
        if laContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: .none) {
            return laContext.biometryType == LABiometryType.faceID
        }
        return false
    }

}

import CoreMotion

class SensorService {

    private let dataHandler: DataHandler
    private let motionManager = CMMotionManager()
    private let updateInterval = 0.05
    private let accelerationDataFileName = "AccelerationData"
    private let gyroscopeDataFileName = "GyroscopeData"
    private let magnetometerDataFileName = "MagnetometerData"
    private let deviceAttitudeFileName = "DeviceAttitude"
    private let deviceRotationRateFileName = "DeviceRotationRate"
    private let deviceGravityFileName = "DeviceGravity"
    private let deviceUserAccelerationFileName = "DeviceUserAcceleration"

    private var accelerometerData: [[Double]] = []
    private var gyroscopeData: [[Double]] = []
    private var magnetometerData: [[Double]] = []
    private var deviceAttitude: [[Double]] = []
    private var deviceRotationRate: [[Double]] = []
    private var deviceGravity: [[Double]] = []
    private var deviceUserAcceleration: [[Double]] = []
    private var attemptNumber = Int()

    init(dataHandler: DataHandler) {
        self.dataHandler = dataHandler
    }

    func startAllSensors(attemptNumber: Int) {
        self.attemptNumber = attemptNumber
        startAccelerometer()
        startGyroscope()
        startMagnetometer()
        startDeviceMotion()
    }

    func stopAllSensors() {
        stopAccelerometer()
        stopGyroscope()
        stopMagnetometer()
        stopDeviceMotion()
    }

    func saveAllSensorData() {
        saveAccelerometerData()
        saveGyroscopeData()
        saveMagnetometerData()
        saveDeviceMotionData()
    }

    private func startAccelerometer() {
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = updateInterval
            motionManager.startAccelerometerUpdates(to: OperationQueue.current!, withHandler: { data, error in
                let timestamp = Date().timeIntervalSince1970
                guard let data = data else {
                    return
                }
                let acceleration = data.acceleration
                self.accelerometerData.append([acceleration.x, acceleration.y, acceleration.z, timestamp])
            })
        }
    }

    private func stopAccelerometer() {
        motionManager.stopAccelerometerUpdates()
    }

    private func saveAccelerometerData() {
        dataHandler.saveSensorDataToFile(attemptNumber: attemptNumber, fileName: accelerationDataFileName, arrayData: accelerometerData)
    }

    private func startGyroscope() {
        if motionManager.isGyroAvailable {
            motionManager.gyroUpdateInterval = updateInterval
            motionManager.startGyroUpdates(to: OperationQueue.current!, withHandler: { data, error in
                let timestamp = Date().timeIntervalSince1970
                guard let data = data else {
                    return
                }
                let gyroscope = data.rotationRate
                self.gyroscopeData.append([gyroscope.x, gyroscope.y, gyroscope.z, timestamp])
            })
        }
    }

    private func stopGyroscope() {
        motionManager.stopGyroUpdates()
    }

    private func saveGyroscopeData() {
        dataHandler.saveSensorDataToFile(attemptNumber: attemptNumber, fileName: gyroscopeDataFileName, arrayData: gyroscopeData)
    }

    private func startMagnetometer() {
        if motionManager.isMagnetometerAvailable {
            motionManager.magnetometerUpdateInterval = updateInterval
            motionManager.startMagnetometerUpdates(to: OperationQueue.current!, withHandler: { data, error in
                let timestamp = Date().timeIntervalSince1970
                guard let data = data else {
                    return
                }
                let magnetometer = data.magneticField
                self.magnetometerData.append([magnetometer.x, magnetometer.y, magnetometer.z, timestamp])
            })
        }
    }

    private func stopMagnetometer() {
        motionManager.stopMagnetometerUpdates()
    }

    private func saveMagnetometerData() {
        dataHandler.saveSensorDataToFile(attemptNumber: attemptNumber, fileName: magnetometerDataFileName, arrayData: magnetometerData)
    }

    private func startDeviceMotion() {
        if motionManager.isDeviceMotionAvailable {
            motionManager.deviceMotionUpdateInterval = updateInterval
            motionManager.startDeviceMotionUpdates(to: OperationQueue.current!, withHandler: { data, error in
                let timestamp = Date().timeIntervalSince1970
                guard let data = data else {
                    return
                }
                let attitudeData = data.attitude
                self.deviceAttitude.append([attitudeData.pitch, attitudeData.roll, attitudeData.yaw, timestamp])
                let rotationRateData = data.rotationRate
                self.deviceRotationRate.append([rotationRateData.x, rotationRateData.y, rotationRateData.z, timestamp])
                let gravityData = data.gravity
                self.deviceGravity.append([gravityData.x, gravityData.y, gravityData.z, timestamp])
                let userAccelerationData = data.userAcceleration
                self.deviceUserAcceleration.append([userAccelerationData.x, userAccelerationData.y, userAccelerationData.z, timestamp])
            })
        }
    }

    private func stopDeviceMotion() {
        motionManager.stopDeviceMotionUpdates()
    }

    private func saveDeviceMotionData() {
        dataHandler.saveSensorDataToFile(attemptNumber: attemptNumber, fileName: deviceAttitudeFileName, arrayData: deviceAttitude)
        dataHandler.saveSensorDataToFile(attemptNumber: attemptNumber, fileName: deviceRotationRateFileName, arrayData: deviceRotationRate)
        dataHandler.saveSensorDataToFile(attemptNumber: attemptNumber, fileName: deviceGravityFileName, arrayData: deviceGravity)
        dataHandler.saveSensorDataToFile(attemptNumber: attemptNumber, fileName: deviceUserAccelerationFileName, arrayData: deviceUserAcceleration)
    }

}

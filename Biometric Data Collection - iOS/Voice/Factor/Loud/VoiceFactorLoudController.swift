import UIKit

class VoiceFactorLoudController: VoiceController {

    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var voicePhrase: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Factor - Loud"

    private let fingerprintFactorWetStoryboard = "FingerprintFactorWet"
    private let sessionOneHomeStoryboard = "SessionOneHome"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func recordAudio(_ sender: Any) {
        recordAudio(counterLabel: counterLabel, voicePhrase: voicePhrase, recordButton: recordButton, nextButton: nextButton)
    }

    @IBAction func nextStoryboard(_ sender: Any) {
        var storyboard = UIStoryboard(name: fingerprintFactorWetStoryboard, bundle: nil)
        if DeviceHandler.isFaceID() {
            storyboard = UIStoryboard(name: sessionOneHomeStoryboard, bundle: nil)
        }
        let viewController = storyboard.instantiateInitialViewController()!
        present(viewController, animated: true, completion: nil)
    }

}

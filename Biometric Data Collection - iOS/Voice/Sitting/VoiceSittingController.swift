import UIKit

class VoiceSittingController: VoiceController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var voicePhrase: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Sitting"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func recordAudio(_ sender: Any) {
        recordAudio(counterLabel: counterLabel, voicePhrase: voicePhrase, recordButton: recordButton, nextButton: nextButton)
    }

}

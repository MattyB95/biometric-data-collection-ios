import UIKit

class VoiceTreadmillController: VoiceController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var voicePhrase: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!

    private let scenario = "Treadmill"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, scenario: scenario)
    }

    @IBAction func recordAudio(_ sender: Any) {
        recordAudio(counterLabel: counterLabel, voicePhrase: voicePhrase, recordButton: recordButton, nextButton: nextButton)
    }

}

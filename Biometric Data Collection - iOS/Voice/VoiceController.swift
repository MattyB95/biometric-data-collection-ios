import UIKit

class VoiceController: UIViewController {

    private let modality = "Voice"
    private let startRecordingText = "Start Recording"
    private let stopRecordingText = "Stop Recording"
    private let minimumAttemptNumber = 5

    private var audioHandler: AudioHandler?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(coder aDecoder: NSCoder, scenario: String) {
        audioHandler = AudioHandler(modality: modality, scenario: scenario)
        super.init(coder: aDecoder)!
    }

    func recordAudio(counterLabel: UILabel, voicePhrase: UILabel, recordButton: UIButton, nextButton: UIButton) {
        let incrementCounter = Int(counterLabel.text!)! + 1
        if recordButton.currentTitle!.isEqual(startRecordingText) {
            updateRecordingUI(recordButton: recordButton, counterLabel: counterLabel, voicePhrase: voicePhrase)
            audioHandler!.startRecording(attemptNumber: incrementCounter)
        } else {
            audioHandler!.stopRecording()
            updateCounterView(counterLabel: counterLabel, counterValue: incrementCounter)
            showNextButton(nextButton: nextButton, counterValue: incrementCounter)
            updateWaitingUI(recordButton: recordButton, counterLabel: counterLabel, voicePhrase: voicePhrase)
        }
    }

    private func updateRecordingUI(recordButton: UIButton, counterLabel: UILabel, voicePhrase: UILabel) {
        recordButton.setTitle(stopRecordingText, for: .normal)
        recordButton.setTitleColor(.label, for: .normal)
        recordButton.backgroundColor = .systemRed
        counterLabel.isHidden = true
        voicePhrase.isHidden = false
    }

    private func updateWaitingUI(recordButton: UIButton, counterLabel: UILabel, voicePhrase: UILabel) {
        recordButton.setTitle(startRecordingText, for: .normal)
        recordButton.setTitleColor(.systemBlue, for: .normal)
        recordButton.backgroundColor = .systemGreen
        counterLabel.isHidden = false
        voicePhrase.isHidden = true
    }

    private func updateCounterView(counterLabel: UILabel, counterValue: Int) {
        counterLabel.text = String(counterValue)
    }

    private func showNextButton(nextButton: UIButton, counterValue: Int) {
        if counterValue >= minimumAttemptNumber {
            nextButton.isHidden = false
        }
    }

}
